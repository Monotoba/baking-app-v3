package us.sensornet.bakingappv3;

/*
 * Attribution: The following code was taking
 * from a youtube video presentation on Espresso
 * conducted by Chiu-ki Chan. The presentation can
 * be be found at:
 *    https://www.youtube.com/watch?v=JlHJFZvZyxw
 */

import android.support.test.espresso.IdlingRegistry;

import com.jakewharton.espresso.OkHttp3IdlingResource;

import okhttp3.OkHttpClient;

public abstract class IdlingResources {

    public static void registerOkHttp(OkHttpClient client) {
        IdlingRegistry.getInstance().register(OkHttp3IdlingResource.create("okhttp", client));
    }
}
