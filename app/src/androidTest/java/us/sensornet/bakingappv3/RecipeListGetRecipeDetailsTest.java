package us.sensornet.bakingappv3;

import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.Toolbar;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import us.sensornet.bakingappv3.ui.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static java.lang.Thread.sleep;
import static org.hamcrest.CoreMatchers.is;

/**
 * Confirms that clicking a recipe in the home screen
 * results in the recipes detail view being displayed.
 * Here we match the toolbar title to guarantee that
 * the proper recipe details are displayed.
 */
@RunWith(AndroidJUnit4.class)
public class RecipeListGetRecipeDetailsTest {
    private static final String TAG = "RecipeListGetRecipeDeta";

    @Rule
    public ActivityTestRule<MainActivity> mActivity = new ActivityTestRule<>(MainActivity.class);

    private static ViewInteraction matchToolbarTitle(CharSequence title) {
        return onView(isAssignableFrom(Toolbar.class)).check(matches(withToolbarTitle(is(title))));
    }

    private static Matcher<Object> withToolbarTitle(final Matcher<CharSequence> textMatcher) {
        return new BoundedMatcher<Object, Toolbar>(Toolbar.class) {
            @Override
            public boolean matchesSafely(Toolbar toolbar) {
                return textMatcher.matches(toolbar.getTitle());
            }

            @Override
            public void describeTo(org.hamcrest.Description description) {
                description.appendText("with toolbar title: ");
                textMatcher.describeTo(description);
            }
        };
    }

    @Test
    public void clickRecipe() {

        // Idling Resource not working on real hardware
        // So add delay to be safe.
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(ViewMatchers.withId(R.id.rv_recipe_list_recycler_view)).perform(RecyclerViewActions.actionOnItemAtPosition(3, click()));

        // Idling Resource not working on real hardware
        // So add delay to be safe.
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        matchToolbarTitle("Cheesecake");
    }

}
