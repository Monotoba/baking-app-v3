package us.sensornet.bakingappv3;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import us.sensornet.bakingappv3.ui.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static java.lang.Thread.sleep;

/**
 *
 */
@RunWith(AndroidJUnit4.class)
public class BakingAppHomePageRecipeSelectTest {
    private static final String TAG = "BakingAppHomePageRecipe";


    @Rule
    public ActivityTestRule<MainActivity> mainActivityTestRule = new ActivityTestRule<>(MainActivity.class);


    @Test
    public void ClickRecipeToSeeDetails() {

        // Idling Resource not working on real hardware
        // So add delay to be safe.
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(ViewMatchers.withId(R.id.rv_recipe_list_recycler_view)).perform(RecyclerViewActions.actionOnItemAtPosition(2, click()));

        // Idling Resource not working on real hardware
        // So add delay to be safe.
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.recipe_ingredients_container)).check(matches(isDisplayed()));

    }

}

