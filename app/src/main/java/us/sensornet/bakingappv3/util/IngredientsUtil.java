package us.sensornet.bakingappv3.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import us.sensornet.bakingappv3.R;
import us.sensornet.bakingappv3.constants.Constants;
import us.sensornet.bakingappv3.model.Ingredient;

public class IngredientsUtil {
    private static final String TAG = "IngredientsUtil";

    private static Context mContext;
    private static SharedPreferences sharedPrefs;
    private static List<Ingredient> mIngredientList = new ArrayList<>();
    private static String mRecipeName = "";

    public static String getRecipeName() {
        if (mRecipeName.isEmpty()) {
            mRecipeName = sharedPrefs.getString(Constants.SELECTED_RECIPE_NAME_KEY, mContext.getString(R.string.appwidget_text));
        }

        return mRecipeName;
    }

    public static List<Ingredient> buildIngredientList(Context context) {
        mContext = context;
        Gson gson = new Gson();

        sharedPrefs = mContext.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE, Context.MODE_PRIVATE);
        mRecipeName = sharedPrefs.getString(Constants.SELECTED_RECIPE_NAME_KEY, mContext.getString(R.string.appwidget_text));

        // Get string set of json string objects
        Set<String> strSet = sharedPrefs.getStringSet(Constants.INGREDIENTS_KEY, null);
        // Convert to array list for processing
        ArrayList<String> ingList = new ArrayList<>(strSet);

        // Convert json strings to list of Ingredient Objects
        for (String item : ingList) {
            Ingredient ing = gson.fromJson(item, Ingredient.class);
            mIngredientList.add(ing);
        }

        return mIngredientList;

    }


    public static String buildIngredientStringList(Context context) {
        String strIngredients = new String("");
        if (null != mIngredientList) {

            for (Ingredient ing : mIngredientList) {

                strIngredients = strIngredients.concat(("\u2022 " + ing.getIngredient().substring(0, 1).toUpperCase() + ing.getIngredient().substring(1) + "\n") + ("\t\t\t Quantity: " + ing.getQuantity().toString() + "\n") + ("\t\t\t Measure: " + ing.getMeasure() + "\n\n"));
            }

        } else {
            Log.d(TAG, "onCreateView: mIngredients is null");
        }

        return strIngredients;
    }
}
