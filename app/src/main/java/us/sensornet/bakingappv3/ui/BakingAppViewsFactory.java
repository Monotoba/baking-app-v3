package us.sensornet.bakingappv3.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import us.sensornet.bakingappv3.R;
import us.sensornet.bakingappv3.constants.Constants;
import us.sensornet.bakingappv3.model.Ingredient;

public class BakingAppViewsFactory implements RemoteViewsService.RemoteViewsFactory {
    private static final String TAG = "BakingAppViewsFactory";

    private static Context mContext;
    private static SharedPreferences mPrefs;
    private static List<Ingredient> mIngredients = new ArrayList<>();


    public BakingAppViewsFactory(Context context, Intent intent) {
        mContext = context;
        mIngredients = buildIngredientList();
    }

    private static List<Ingredient> buildIngredientList() {
        Gson gson = new Gson();

        mPrefs = mContext.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE, Context.MODE_PRIVATE);

        // Get string set of json string objects
        Set<String> strSet = mPrefs.getStringSet(Constants.INGREDIENTS_KEY, null);
        // Convert to array list for processing
        ArrayList<String> ingList = new ArrayList<>(strSet);

        // Convert json strings to list of Ingredient Objects
        for (String item : ingList) {
            Ingredient ing = gson.fromJson(item, Ingredient.class);
            mIngredients.add(ing);
        }

        return mIngredients;

    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {
        if (null == mIngredients) {
            mIngredients = new ArrayList<Ingredient>();
        }
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        return (null == mIngredients) ? 0 : mIngredients.size();
    }

    @Override
    public RemoteViews getViewAt(int position) {
        Log.d(TAG, "getViewAt: " + String.valueOf(position) + mIngredients.get(position).getIngredient() + ": " + mIngredients.get(position).getMeasure() + " " + mIngredients.get(position).getQuantity() + "");

        if ((position == AdapterView.INVALID_POSITION) || (null == mIngredients) || (0 == mIngredients.size())) {
            return null;
        }

        RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.widget_list_item);


        rv.setTextViewText(R.id.appwidget_text, makeIngredientString(mIngredients.get(position)));

        return rv;

    }

    // Build up a string for the ingredient list
    private String makeIngredientString(Ingredient ing) {

        String strIngredient = ing.getIngredient().substring(0, 1).toUpperCase().concat(ing.getIngredient().substring(1)) + ":\n\t" + ing.getQuantity() + " " + ing.getMeasure();

        return strIngredient;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return (long) position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
