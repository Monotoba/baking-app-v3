package us.sensornet.bakingappv3.ui;

import android.os.Bundle;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import us.sensornet.bakingappv3.R;
import us.sensornet.bakingappv3.constants.Constants;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    CountingIdlingResource idlingResource = new CountingIdlingResource("MAIN_ACTIVITY_LOADER");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // TODO: clean up logs
        Log.d(TAG, "MainActivity called");

        // Get orientation
        int screenOrientation = getResources().getConfiguration().orientation;

        IdlingRegistry.getInstance().register(idlingResource);
        idlingResource.increment();

        // Load fragment if not already loaded
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(Constants.FRAGMENT_RECIPE_LIST);
        if (null == fragment) {
            Fragment recipeListFragment = new RecipeListFragment();
            fragmentManager.beginTransaction().replace(R.id.recipe_list_container, recipeListFragment, Constants.FRAGMENT_RECIPE_LIST).commit();
        }

        idlingResource.decrement();
        IdlingRegistry.getInstance().unregister(idlingResource);
    }

}
