package us.sensornet.bakingappv3.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import us.sensornet.bakingappv3.R;
import us.sensornet.bakingappv3.constants.Constants;
import us.sensornet.bakingappv3.util.IngredientsUtil;

/**
 * This activity is openned by the widget and simply
 * displays a list of ingredients for the last viewed
 * recipe.
 */
public class IngredientsActivity extends AppCompatActivity {
    private String TAG = getClass().getSimpleName();

    private SharedPreferences sharedPrefs;
    private String strIngredients = "";
    private String strRecipeName = "";
    private int intRecipeIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ingradients);

        sharedPrefs = this.getSharedPreferences(Constants.SHARED_PREFERENCE_FILE, MODE_PRIVATE);

        intRecipeIndex = sharedPrefs.getInt(Constants.SELECTED_RECIPE_INDEX_KEY, 999);
        //strIngredients = sharedPrefs.getString(Constants.INGREDIENTS_KEY, getString(R.string.default_recipe_select_msg));
        strRecipeName = sharedPrefs.getString(Constants.SELECTED_RECIPE_NAME_KEY, getString(R.string.app_name));
        TextView tvRecipeName = findViewById(R.id.tv_ingredients_activity_title);
        TextView tvIngredients = findViewById(R.id.tv_ingredients);


        tvRecipeName.setText(strRecipeName);
        tvIngredients.setText(IngredientsUtil.buildIngredientStringList(this));
    }
}
