package us.sensornet.bakingappv3.ui;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;
import android.widget.RemoteViews;

import java.util.ArrayList;
import java.util.List;

import us.sensornet.bakingappv3.R;
import us.sensornet.bakingappv3.model.Ingredient;
import us.sensornet.bakingappv3.util.IngredientsUtil;

/**
 * Implementation of App Widget functionality.
 */
public class BakingTimeAppWidget extends AppWidgetProvider {
    private static final String TAG = "BakingTimeAppWidget";

    private static Context mContext;
    private static SharedPreferences sharedPrefs;
    private static String mRecipeName = "";
    private static String strIngredient;
    private static List<Ingredient> mListIngredients = new ArrayList<>();
    private static List<Ingredient> mIngredientList = new ArrayList<>();

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        Log.d(TAG, "updateAppWidget: called");
        mContext = context;

        // Get ingredients
        mIngredientList = IngredientsUtil.buildIngredientList(mContext);
        mRecipeName = IngredientsUtil.getRecipeName();

        // Setting adapter to listView for the widget
        RemoteViews remoteViews = new RemoteViews(mContext.getPackageName(), R.layout.baking_time_app_widget);

        // Set recipe name
        remoteViews.setTextViewText(R.id.tv_widget_title, mRecipeName + " " + mContext.getString(R.string.recipe_name_ingredient_tag));

        // RemoteService needed to provide adapter
        Intent srcIntent = new Intent(mContext, BakingAppRemoteViewService.class);

        // Passing app widget id to that RemoteViews Service
        srcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

        // Setting a unique Uri to the intent
        srcIntent.setData(Uri.parse(srcIntent.toUri(Intent.URI_INTENT_SCHEME)));
        remoteViews.setRemoteAdapter(appWidgetId, R.id.lv_widget_list, srcIntent);

        // setting an empty view in case of no data
        remoteViews.setEmptyView(R.id.lv_widget_list, R.id.tv_empty_list);

        // Set click handler to open ingredient list
        Intent intent = new Intent(context, IngredientsActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        PendingIntent pendingServiceIntent = PendingIntent.getService(context, 0, srcIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.tv_widget_title, pendingIntent);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
    }


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

}

