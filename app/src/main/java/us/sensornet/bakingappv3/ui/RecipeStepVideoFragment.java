package us.sensornet.bakingappv3.ui;


import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import us.sensornet.bakingappv3.R;
import us.sensornet.bakingappv3.constants.Constants;
import us.sensornet.bakingappv3.model.Step;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeStepVideoFragment extends Fragment {
    private static final String TAG = "RecipeStepVideoFragment";

    CountingIdlingResource idlingResource = new CountingIdlingResource("STEP_VIDEO_LOADER");

    // Exoplayer
    private Handler mHandler;
    private BandwidthMeter mBandwidthMeter;
    private TrackSelection.Factory mTrackSelectionFactory;
    private TrackSelector mTrackSelector;
    private SimpleExoPlayer mPlayer;
    private SimpleExoPlayerView mSimpleExoPlayerView;
    private DefaultDataSourceFactory mDataSourceFactory;

    private String mVideoUrl;
    private String mThumbnailUrl;
    private Step mStep;

    private int mScreenOrientation;

    private Long mVideoPosition = 0L;
    private boolean mPlayerState = false;
    private int mPlayerWindow = 0;

    public RecipeStepVideoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mVideoPosition = C.TIME_UNSET;

        idlingResource.increment();
        IdlingRegistry.getInstance().register(idlingResource);

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_recipe_step_video, container, false);
        TextView tvStepDesc = rootView.findViewById(R.id.recipe_step_detail_text);

        mScreenOrientation = getResources().getConfiguration().orientation;

        // Get thumbnailUrl
        // Get videoUrl
        Bundle bundle = getArguments();
        mVideoUrl = bundle.getString(Constants.VIDEO_URL_KEY);
        mThumbnailUrl = bundle.getString(Constants.THUMBNAIL_URL_KEY);
        mStep = Parcels.unwrap(bundle.getParcelable(Constants.STEP_KEY));

        Log.d(TAG, "onCreateView: >>> Getting video position: " + String.valueOf(mVideoPosition));


        if (mScreenOrientation == Configuration.ORIENTATION_PORTRAIT) {
            if (null != mStep) {
                tvStepDesc.setText("* " + mStep.getShortDescription() + "\n" + mStep.getDescription());
            } else {
                Log.d(TAG, "onCreateView: mStep is null");
            }
        }


        // Handle thumbnail
        if (null != mThumbnailUrl && !mThumbnailUrl.isEmpty()) {

            Uri thumbUri = Uri.parse(mThumbnailUrl);
            ImageView imgThumbnail = rootView.findViewById(R.id.img_thumbnail);
            Picasso.get().load(thumbUri).into(imgThumbnail);
        }

        // if, videoUrl != null
        //   Create and load the player
        // else
        //   Copy step detail description into a text view
        if (mScreenOrientation == Configuration.ORIENTATION_PORTRAIT) {
            if (null != mVideoUrl && !mVideoUrl.isEmpty()) {
                // Hide notice message
                View msgView = rootView.findViewById(R.id.tv_notice_message);
                if (null != msgView) {
                    msgView.setVisibility(View.GONE);
                }
                // Show player
                mSimpleExoPlayerView = rootView.findViewById(R.id.player_view);
                if (null == mPlayer) {
                    createPlayer();
                }
            } else {
                Log.d(TAG, "onCreateView: mVideoUrl is null or empty");
                Log.d(TAG, "onCreateView: mVideoUrl: " + mVideoUrl);

                // Hide player
                View view = rootView.findViewById(R.id.player_view);
                view.setVisibility(View.GONE);
            }

        } else if (mScreenOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (null != mVideoUrl && !mVideoUrl.isEmpty()) {
                // Hide notice message
                TextView msgView = rootView.findViewById(R.id.tv_notice_message);
                msgView.setVisibility(View.GONE);
                // Show player
                mSimpleExoPlayerView = rootView.findViewById(R.id.player_view);

                if (null == mPlayer) {
                    createPlayer();
                }

                // Set step text for tablet mode
                if (getResources().getConfiguration().isLayoutSizeAtLeast(Configuration.SCREENLAYOUT_SIZE_LARGE)) {
                    // Note: tv_recipe_step_text only exists in LARGE screens and up!=
                    TextView stepTextView = getActivity().findViewById(R.id.tv_recipe_step_text);

                    if (null != stepTextView) {
                        stepTextView.setVisibility(View.VISIBLE);
                        stepTextView.setText("* " + mStep.getShortDescription() + "\n" + mStep.getDescription());

                        msgView = rootView.findViewById(R.id.tv_notice_message);
                        msgView.setVisibility(View.GONE);

                    } else {
                        Log.d(TAG, "onCreateView: Called; tv_recipe_step_text does not exists in large screen layout");
                    }
                }

            } else {
                Log.d(TAG, "onCreateView: mVideoUrl is null or empty");
                Log.d(TAG, "onCreateView: mVideoUrl: " + mVideoUrl);

                // Hide player
                View vidView = rootView.findViewById(R.id.player_view);
                vidView.setVisibility(View.GONE);
                // Show notice message
                TextView msgView = rootView.findViewById(R.id.tv_notice_message);
                msgView.setVisibility(View.VISIBLE);
            }
        }

        idlingResource.decrement();
        IdlingRegistry.getInstance().unregister(idlingResource);

        return rootView;
    }


    private void createPlayer() {
        Log.d(TAG, "createPlayer: called");
        if (null == mPlayer) {
            // Create a default TrackSelector
            mHandler = new Handler();
            mBandwidthMeter = new DefaultBandwidthMeter();
            mTrackSelectionFactory = new AdaptiveTrackSelection.Factory(mBandwidthMeter);
            mTrackSelector = new DefaultTrackSelector(mTrackSelectionFactory);
            mDataSourceFactory = new DefaultDataSourceFactory(getActivity(), Util.getUserAgent(getContext(), "Baking App"));
            mPlayer = ExoPlayerFactory.newSimpleInstance(getContext(), mTrackSelector);
            mSimpleExoPlayerView.setPlayer(mPlayer);

            Uri mediaUri = Uri.parse(mVideoUrl);
            ExtractorMediaSource mMediaSource = new ExtractorMediaSource.Factory(mDataSourceFactory).createMediaSource(mediaUri);

            mPlayer.prepare(mMediaSource);

            // hide controls if in landscape mode after 1/2 second
            // and set to full screen mode
            if (mScreenOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                // Hide the player controls after 1/2 second
                mSimpleExoPlayerView.setControllerShowTimeoutMs(500);

                // Expand the video player as large as possible
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mSimpleExoPlayerView.getLayoutParams();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                params.height = ViewGroup.LayoutParams.MATCH_PARENT;
                params.bottomMargin = 0;
                params.topMargin = 0;
                params.leftMargin = 0;
                params.rightMargin = 0;

                mSimpleExoPlayerView.setLayoutParams(params);

                // Hide action bar if exists
                if (null != ((AppCompatActivity) getActivity()).getSupportActionBar()) {
                    ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
                }

                // If tablet?

            } else {
                // Set video player layout for portrait mode
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mSimpleExoPlayerView.getLayoutParams();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                params.height = 600;
                mSimpleExoPlayerView.setLayoutParams(params);

                // Show the actionbar if exists
                if (null != ((AppCompatActivity) getActivity()).getSupportActionBar()) {
                    ((AppCompatActivity) getActivity()).getSupportActionBar().show();
                }
            }

            mPlayer.setPlayWhenReady(true);

        } else {
            // Restore Player State and Position
            mPlayer.setPlayWhenReady(mPlayerState);
            mPlayer.seekTo(mPlayerWindow, mVideoPosition);
        }

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (null != savedInstanceState) {
            mVideoPosition = savedInstanceState.getLong(Constants.VIDEO_POSITION);
            mPlayerState = savedInstanceState.getBoolean(Constants.PLAYER_STATE_KEY);
            mPlayerWindow = savedInstanceState.getInt(Constants.PLAYER_WINDOW_KEY);
            createPlayer();
        }

    }



    @Override
    public void onStop() {
        super.onStop();
        if (mPlayer != null) {
            mVideoPosition = mPlayer.getCurrentPosition();
            mPlayerState = mPlayer.getPlayWhenReady();
            mPlayerWindow = mPlayer.getCurrentAdIndexInAdGroup();
            mPlayer.stop();
            if (Util.SDK_INT > 23) {
                mPlayer.release();
                mPlayer = null;
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mPlayer != null) {
            mVideoPosition = mPlayer.getCurrentPosition();
            mPlayerState = mPlayer.getPlayWhenReady();
            mPlayerWindow = mPlayer.getCurrentAdIndexInAdGroup();
            mPlayer.stop();
            if (Util.SDK_INT <= 23) {
                mPlayer.release();
                mPlayer = null;
            }
        }
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(Constants.VIDEO_POSITION, mVideoPosition);
        outState.putBoolean(Constants.PLAYER_STATE_KEY, mPlayerState);
    }


}
