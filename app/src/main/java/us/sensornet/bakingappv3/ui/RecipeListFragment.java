package us.sensornet.bakingappv3.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import us.sensornet.bakingappv3.R;
import us.sensornet.bakingappv3.adapter.RecipeListAdapter;
import us.sensornet.bakingappv3.constants.Constants;
import us.sensornet.bakingappv3.model.Recipe;
import us.sensornet.bakingappv3.repository.RecipeRepository;

import static us.sensornet.bakingappv3.constants.Constants.SELECTED_RECIPE_NAME_KEY;


public class RecipeListFragment extends Fragment implements RecipeListAdapter.OnItemClicked {
    private static final String TAG = "RecipeListFragment";

    CountingIdlingResource idlingResource = new CountingIdlingResource("DATA_LOADER");

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecipeListAdapter mRecipeListAdapter;
    private RecipeRepository mRecipeRepository;
    private List<Recipe> mRecipes;

    public RecipeListFragment() {
        // Required empty public constructor
    }


    public static RecipeListFragment newInstance() {
        RecipeListFragment fragment = new RecipeListFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        IdlingRegistry.getInstance().register(idlingResource);
        idlingResource.increment();

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_recipe_list, container, false);

        // Get orientation
        int screenOrientation = getResources().getConfiguration().orientation;

        if (screenOrientation == Configuration.ORIENTATION_PORTRAIT) {

            mRecyclerView = rootView.findViewById(R.id.rv_recipe_list_recycler_view);
            mLayoutManager = new LinearLayoutManager(getContext());
            mRecyclerView.setLayoutManager(mLayoutManager);

            mRecipeListAdapter = new RecipeListAdapter(getContext());
            mRecipeListAdapter.setOnClick(this); // Bind the listener

            mRecipeRepository = new RecipeRepository(mRecipeListAdapter);
            mRecyclerView.setAdapter(mRecipeListAdapter);

        } else if (screenOrientation == Configuration.ORIENTATION_LANDSCAPE) {

            mRecyclerView = rootView.findViewById(R.id.rv_recipe_list_recycler_view);
            mLayoutManager = new GridLayoutManager(getContext(), 3);
            mRecyclerView.setLayoutManager(mLayoutManager);

            mRecipeListAdapter = new RecipeListAdapter(getContext());
            mRecipeListAdapter.setOnClick(this); // Bind the listener

            mRecipeRepository = new RecipeRepository(mRecipeListAdapter);
            mRecyclerView.setAdapter(mRecipeListAdapter);
        }

        idlingResource.decrement();
        IdlingRegistry.getInstance().unregister(idlingResource);
        return rootView;
    }

    @Override
    public void onItemClick(int position) {
        // The onClick implementation of the RecyclerView item click
        // intent code here
        // Save selected recipe
        IdlingRegistry.getInstance().register(idlingResource);
        idlingResource.increment();

        // Get latest data
        mRecipes = mRecipeListAdapter.getRecipeListData();
        Bundle selectedRecipeBundle = new Bundle();
        ArrayList<Recipe> selectedRecipe = new ArrayList<>();

        idlingResource.decrement();
        IdlingRegistry.getInstance().unregister(idlingResource);

        Recipe rxp = mRecipes.get(position);
        Log.d(TAG, "Recipe is: --- " + rxp.getName());

        Parcelable recipe = Parcels.wrap(rxp);
        selectedRecipeBundle.putParcelable(Constants.SELECTED_RECIPE_KEY, Parcels.wrap(rxp));

        Intent detailIntent = new Intent(getActivity(), RecipeDetailActivity.class);

        selectedRecipeBundle.putInt(Constants.SELECTED_RECIPE_INDEX_KEY, position);
        selectedRecipeBundle.putString(SELECTED_RECIPE_NAME_KEY, mRecipes.get(position).getName());

        detailIntent.putExtras(selectedRecipeBundle);
        startActivity(detailIntent);

    }

    // Used for debugging
    private void dumpRecipes() {
        if (null != mRecipes) {

            for (Recipe recipe : mRecipes) {
                Log.d(TAG, "Recipe @ index: " + mRecipes.indexOf(recipe) + " is: " + recipe.getName());
            }
        } else {
            Log.d(TAG, "mRecipes is null");
        }
    }


}
