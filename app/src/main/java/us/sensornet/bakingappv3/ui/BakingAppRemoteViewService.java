package us.sensornet.bakingappv3.ui;

import android.content.Intent;
import android.widget.RemoteViewsService;

public class BakingAppRemoteViewService extends RemoteViewsService {
    private static final String TAG = "BakingAppRemoteViewServ";

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new BakingAppViewsFactory(this.getApplicationContext(), intent);
    }
}
