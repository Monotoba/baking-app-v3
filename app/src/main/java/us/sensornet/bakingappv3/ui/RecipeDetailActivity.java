package us.sensornet.bakingappv3.ui;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;

import org.parceler.Parcels;

import java.util.HashSet;
import java.util.List;

import us.sensornet.bakingappv3.R;
import us.sensornet.bakingappv3.constants.Constants;
import us.sensornet.bakingappv3.model.Ingredient;
import us.sensornet.bakingappv3.model.Recipe;

import static us.sensornet.bakingappv3.constants.Constants.FRAGMENT_INGREDIENTS_LIST;
import static us.sensornet.bakingappv3.constants.Constants.FRAGMENT_STEPS_LIST;
import static us.sensornet.bakingappv3.constants.Constants.FRAGMENT_STEPS_VIDEO;
import static us.sensornet.bakingappv3.constants.Constants.SELECTED_RECIPE_INDEX_KEY;
import static us.sensornet.bakingappv3.constants.Constants.SELECTED_RECIPE_KEY;
import static us.sensornet.bakingappv3.constants.Constants.SELECTED_RECIPE_NAME_KEY;
import static us.sensornet.bakingappv3.constants.Constants.SHARED_PREFERENCE_FILE;


public class RecipeDetailActivity extends AppCompatActivity {
    private static final String TAG = "RecipeDetailActivity";

    CountingIdlingResource idlingResource = new CountingIdlingResource("RecipeDataActivity");

    public List<Ingredient> mIngredients;
    FragmentManager mFragmentManager;
    private List<Recipe> mRecipes;
    private String mRecipeName = "";
    private int mRecipeIndex = 0;
    private Recipe mSelectedRecipe;
    private SharedPreferences mSharedPrefs;
    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_detail);
        Log.d(TAG, "RecipeDetailActivity onCreate() called");

        idlingResource.increment();
        IdlingRegistry.getInstance().register(idlingResource);

        mSharedPrefs = this.getSharedPreferences(SHARED_PREFERENCE_FILE, MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedPrefs.edit();

        if (null == mSharedPrefs) {
            Log.d(TAG, "onCreate: mSharedPrefs is null");
        } else {
            Log.d(TAG, "onCreate: mSharedPrefs is not null");
            mRecipeIndex = mSharedPrefs.getInt(SELECTED_RECIPE_INDEX_KEY, mRecipeIndex);
            mRecipeName = mSharedPrefs.getString(SELECTED_RECIPE_NAME_KEY, mRecipeName);
            Log.d(TAG, "onCreate: RecipeName from SharedPrefs: " + mRecipeName);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle selectedRecipeBundle = getIntent().getExtras();

        // Get data passed from previous activity
        // or saved in instance state
        if (null == savedInstanceState) {
            selectedRecipeBundle = getIntent().getExtras();

            mSelectedRecipe = Parcels.unwrap(getIntent().getParcelableExtra(SELECTED_RECIPE_KEY));
            mRecipeIndex = selectedRecipeBundle.getInt(SELECTED_RECIPE_INDEX_KEY);
            mRecipeName = selectedRecipeBundle.getString(SELECTED_RECIPE_NAME_KEY);

        } else {
            Log.d(TAG, "savedInstanceState is not null");
            // TODO swap out intent data for savedInstanceState data
            Bundle data = getIntent().getExtras();

            mSelectedRecipe = Parcels.unwrap(data.getParcelable(SELECTED_RECIPE_KEY));
            mRecipeIndex = data.getInt(SELECTED_RECIPE_INDEX_KEY);
            mRecipeName = data.getString(SELECTED_RECIPE_NAME_KEY);

        }

        mRecipeName = mSelectedRecipe.getName();
        // Set the page title to the recipe name
        setTitle(mRecipeName);

        mIngredients = mSelectedRecipe.getIngredients();
        HashSet<String> ingSet = new HashSet<>();
        Gson gson = new Gson();

        for (Ingredient ing : mIngredients) {
            String strIngredient = gson.toJson(ing);
            //strIngredient = ing.getIngredient().substring(0, 1).toUpperCase() + ing.getIngredient().substring(1) + ":") + ("" + ing.getQuantity().toString() + " ") + (" " + ing.getMeasure() + ""));
            ingSet.add(strIngredient);
        }

        // Save in shared prefs.
        editor.putInt(SELECTED_RECIPE_INDEX_KEY, mRecipeIndex);
        editor.commit();
        editor.putString(SELECTED_RECIPE_NAME_KEY, mRecipeName);
        editor.commit();
        editor.putStringSet(Constants.INGREDIENTS_KEY, ingSet);
        editor.commit();

        // Update the widget view
        updateWidget();

        // Get orientation
        int screenOrientation = getResources().getConfiguration().orientation;

        // Load fragments only if not already loaded
        mFragmentManager = getSupportFragmentManager();
        Fragment fragment = mFragmentManager.findFragmentByTag(FRAGMENT_INGREDIENTS_LIST);

        if (fragment == null) {
            // Go show the data in the ingredients fragment
            loadIngredientsFragment();
        }

        fragment = mFragmentManager.findFragmentByTag(FRAGMENT_STEPS_LIST);
        if (fragment == null) {
            // load the steps list fragment
            loadStepsFragment();
        }


        // Don't show this message on large screen layouts
        if (!getResources().getConfiguration().isLayoutSizeAtLeast(Configuration.SCREENLAYOUT_SIZE_LARGE)) {
            View containerView = findViewById(R.id.recipe_detail_steps_container);
            if (null != containerView) {
                loadStepDetailFragment();
            }
        }

        idlingResource.decrement();
        IdlingRegistry.getInstance().unregister(idlingResource);

    }


    private void updateWidget() {
        Intent intent = new Intent(this, BakingTimeAppWidget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        // Use an array and EXTRA_APPWIDGET_IDS instead of AppWidgetManager.EXTRA_APPWIDGET_ID,
        // since it seems the onUpdate() is only fired on that:
        int[] ids = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(new ComponentName(getApplication(), BakingTimeAppWidget.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        sendBroadcast(intent);
    }

    private void loadIngredientsFragment() {
        IdlingRegistry.getInstance().register(idlingResource);
        idlingResource.increment();

        if (null != mSelectedRecipe) {
            Log.d(TAG, "Selected Recipe is not null");
        } else {
            Log.d(TAG, "Selected Recipe is null");
        }


        Bundle bundle = new Bundle();
        bundle.putParcelable(SELECTED_RECIPE_KEY, Parcels.wrap(mSelectedRecipe));
        bundle.putInt(SELECTED_RECIPE_INDEX_KEY, mRecipeIndex);
        bundle.putString(SELECTED_RECIPE_NAME_KEY, mSelectedRecipe.getName());



        if (null != bundle) {
            Log.d(TAG, "Bundle not null on created fragment\n\n");
            Fragment ingFragment = new RecipeIngredientsFragment();
            ingFragment.setArguments(bundle);

            getSupportFragmentManager().beginTransaction().add(R.id.recipe_ingredients_container, ingFragment, FRAGMENT_INGREDIENTS_LIST).commit();

        } else {
            Log.d(TAG, "Bundle is null in onCreate fragment\n\n");
        }

        idlingResource.decrement();
        IdlingRegistry.getInstance().unregister(idlingResource);
    }

    private void loadStepsFragment() {
        IdlingRegistry.getInstance().register(idlingResource);
        idlingResource.increment();

        Bundle bundle = new Bundle();

        bundle.putParcelable(SELECTED_RECIPE_KEY, Parcels.wrap(mSelectedRecipe));
        bundle.getInt(SELECTED_RECIPE_INDEX_KEY, mRecipeIndex);
        bundle.getString(SELECTED_RECIPE_NAME_KEY, mRecipeName);

        if (null != bundle) {
            Log.d(TAG, "Bundle not null on created fragment\n\n");
            Fragment stepsListFragment = new RecipeStepsListFragment();
            stepsListFragment.setArguments(bundle);

            getSupportFragmentManager().beginTransaction().add(R.id.recipe_steps_container, stepsListFragment, FRAGMENT_STEPS_LIST).commit();

        } else {
            Log.d(TAG, "Bundle is null in onCreate fragment\n\n");
        }

        idlingResource.decrement();
        IdlingRegistry.getInstance().unregister(idlingResource);
    }


    private void loadStepDetailFragment() {
        IdlingRegistry.getInstance().register(idlingResource);
        idlingResource.increment();

        Bundle bundle = new Bundle();

        bundle.putParcelable(SELECTED_RECIPE_KEY, Parcels.wrap(mSelectedRecipe));

        if (null != bundle) {
            Log.d(TAG, "loadStepDetailFragment: bundle not null");
            Fragment stepDetailFragment = new RecipeStepVideoFragment();
            stepDetailFragment.setArguments(bundle);

            mFragmentManager.beginTransaction().replace(R.id.recipe_detail_steps_container, stepDetailFragment, FRAGMENT_STEPS_VIDEO).commit();
        }

        idlingResource.decrement();
        IdlingRegistry.getInstance().unregister(idlingResource);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(SELECTED_RECIPE_KEY, Parcels.wrap(mSelectedRecipe));
        outState.putInt(SELECTED_RECIPE_KEY, mRecipeIndex);
        outState.putString(SELECTED_RECIPE_NAME_KEY, mRecipeName);
    }

}
