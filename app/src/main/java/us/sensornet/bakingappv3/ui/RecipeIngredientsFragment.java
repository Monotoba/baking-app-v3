package us.sensornet.bakingappv3.ui;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import us.sensornet.bakingappv3.R;
import us.sensornet.bakingappv3.constants.Constants;
import us.sensornet.bakingappv3.model.Ingredient;
import us.sensornet.bakingappv3.model.Recipe;


public class RecipeIngredientsFragment extends Fragment {
    private static final String TAG = "RecipeIngredientsFrag";

    private static CountingIdlingResource idlingResource = new CountingIdlingResource("ING_DATA_LOADER");

    public List<Ingredient> mIngredients;
    public String strIngredients = "";
    private List<Recipe> mRecipes;
    private String mRecipeName = "";
    private int mRecipeIndex = 0;
    private Recipe mSelectedRecipe;

    private SharedPreferences sharedPrefs;

    public RecipeIngredientsFragment() {
        // Required empty public constructor
    }


    public static RecipeIngredientsFragment newInstance(Bundle bundle) {
        idlingResource.increment();
        IdlingRegistry.getInstance().register(idlingResource);

        RecipeIngredientsFragment fragment = new RecipeIngredientsFragment();

        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IdlingRegistry.getInstance().register(idlingResource);
        idlingResource.increment();

        // TODO Remove Logs
        Bundle bundle = getArguments();
        if (null != bundle) {
            Log.d(TAG, "onCreate: --- getArguments returned a bundle");
            mRecipes = new ArrayList<>();
            mSelectedRecipe = Parcels.unwrap(bundle.getParcelable(Constants.SELECTED_RECIPE_KEY));
            mRecipeIndex = bundle.getInt(Constants.SELECTED_RECIPE_INDEX_KEY);
            mRecipeName = bundle.getString(Constants.SELECTED_RECIPE_NAME_KEY);

        } else {
            Log.d(TAG, "getArguments returned null");
        }

        if (null == mSelectedRecipe) {
            Log.d(TAG, "onCreate: mSelectedRecipe is null after unpacking");
        } else {
            mIngredients = mSelectedRecipe.getIngredients();
        }

        idlingResource.decrement();
        IdlingRegistry.getInstance().unregister(idlingResource);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        idlingResource.increment();
        IdlingRegistry.getInstance().register(idlingResource);

        // Inflate the layout for this fragment
        View ingredientScrollView = inflater.inflate(R.layout.fragment_recipe_ingredients, container, false);

        TextView ingTextView = ingredientScrollView.findViewById(R.id.tv_ingredients);

        strIngredients = new String("");
        if (null != mIngredients) {

            for (Ingredient ing : mIngredients) {
                strIngredients = strIngredients.concat(("\u2022 " + ing.getIngredient().substring(0, 1).toUpperCase() + ing.getIngredient().substring(1) + "\n") + ("\t\t\t Quantity: " + ing.getQuantity().toString() + "\n") + ("\t\t\t Measure: " + ing.getMeasure() + "\n\n"));
                ingTextView.setText(strIngredients);
            }

        } else {
            Log.d(TAG, "onCreateView: mIngredients is null");
        }

        idlingResource.decrement();
        IdlingRegistry.getInstance().unregister(idlingResource);

        return ingredientScrollView;
    }

}
