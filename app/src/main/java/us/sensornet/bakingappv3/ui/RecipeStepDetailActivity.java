package us.sensornet.bakingappv3.ui;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.exoplayer2.C;

import org.parceler.Parcels;

import java.util.List;

import us.sensornet.bakingappv3.R;
import us.sensornet.bakingappv3.constants.Constants;
import us.sensornet.bakingappv3.model.Recipe;
import us.sensornet.bakingappv3.model.Step;

import static us.sensornet.bakingappv3.constants.Constants.SELECTED_RECIPE_NAME_KEY;

public class RecipeStepDetailActivity extends AppCompatActivity {
    private static final String TAG = "RecipeStepDetailActivit";

    CountingIdlingResource idlingResource = new CountingIdlingResource("STEPS_DATA_LOADER");

    private String mRecipeName;
    private Integer mRecipeIndex;
    private Recipe mSelectedRecipe;
    private Integer mSelectedStepIndex;
    private List<Step> mSteps;

    // Video
    private String mVideoUrl;
    private String mThumbnailUrl;
    private Long mVideoPosition = C.TIME_UNSET;

    private FragmentManager mFragmentManager;
    private Fragment mPlayerFragment;

    private Bundle mSavedInstanceState;

    private int mScreenOrientation;


    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSavedInstanceState = savedInstanceState;

        setContentView(R.layout.activity_recipe_step_detail);

        idlingResource.increment();
        IdlingRegistry.getInstance().register(idlingResource);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();

        mSelectedRecipe = Parcels.unwrap(getIntent().getParcelableExtra(Constants.SELECTED_RECIPE_KEY));
        mSelectedStepIndex = bundle.getInt(Constants.SELECTED_STEP_INDEX_KEY);
        mRecipeIndex = bundle.getInt(Constants.SELECTED_RECIPE_INDEX_KEY);
        mRecipeName = bundle.getString(Constants.SELECTED_RECIPE_NAME_KEY);
        mSteps = mSelectedRecipe.getSteps();
        mThumbnailUrl = mSelectedRecipe.getSteps().get(mSelectedStepIndex).getThumbnailURL();
        mVideoUrl = mSelectedRecipe.getSteps().get(mSelectedStepIndex).getVideoURL();

        //>>>mVideoPosition = savedInstanceState.getLong(Constants.VIDEO_POSITION);

        String sTitle = mRecipeName + ": " + mSteps.get(mSelectedStepIndex).shortDescription.substring(0, 12) + "...";

        setTitle(sTitle);

        mScreenOrientation = getResources().getConfiguration().orientation;

        // Load fragments
        mFragmentManager = getSupportFragmentManager();
        if (mScreenOrientation == Configuration.ORIENTATION_PORTRAIT) {

            // Setup back and next buttons if they exist
            LinearLayout llNavButtonContainer = findViewById(R.id.ll_nav_button_container);
            if (null != llNavButtonContainer) {
                setNavClickHandlers();
            }

            Fragment fragment = mFragmentManager.findFragmentByTag(Constants.FRAGMENT_STEPS_VIDEO);
            if (null == fragment) {
                //mPlayerFragment = getSupportFragmentManager().getFragment(savedInstanceState, Constants.PLAYER_FRAGMENT_KEY);
                loadPlayerFragment(null);
            }

        } else if (mScreenOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            // Load fragments only if not already loaded
            Fragment fragment = mFragmentManager.findFragmentByTag(Constants.FRAGMENT_STEPS_VIDEO);
            if (null == fragment) {
                loadPlayerFragment(null);
            }
        }

        idlingResource.decrement();
        IdlingRegistry.getInstance().unregister(idlingResource);
    }


    private void setNavClickHandlers() {
        ImageButton imgBtnBack = findViewById(R.id.img_button_prev);
        ImageButton imgBtnNext = findViewById(R.id.img_button_next);

        imgBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onNavBtnClick: called Back");
                if (mSelectedStepIndex > 0) {
                    mSelectedStepIndex--;
                    loadPlayerFragment(mSavedInstanceState);
                } else {
                    Log.d(TAG, "onClick: You are at the first step.");
                    showNavNotice(false);
                }
            }
        });

        imgBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onNavBtnClick: called Next");
                if (mSelectedStepIndex < (mSteps.size() - 1)) {
                    mSelectedStepIndex++;
                    loadPlayerFragment(mSavedInstanceState);
                } else {
                    Log.d(TAG, "onClick: There are no more steps");
                    showNavNotice(true);
                }
            }
        });

    }

    private void showNavNotice(Boolean isLast) {
        String msg = "";
        if (isLast) {
            msg = "You are already on the last step.";
        } else {
            msg = "This is the first step.";
        }
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }


    private void loadPlayerFragment(Bundle savedInstanceState) {
        Log.d(TAG, "loadPlayerFragment: called");
        idlingResource.increment();
        IdlingRegistry.getInstance().register(idlingResource);

        // Note: This method assumes the existence
        // of a suitable video url.
        mVideoUrl = mSteps.get(mSelectedStepIndex).getVideoURL();
        mThumbnailUrl = mSteps.get(mSelectedStepIndex).getThumbnailURL();


        Bundle bundle = new Bundle();
        bundle.putString(Constants.VIDEO_URL_KEY, mVideoUrl);
        bundle.putString(Constants.THUMBNAIL_URL_KEY, mThumbnailUrl);
        bundle.putParcelable(Constants.STEP_KEY, Parcels.wrap(mSteps.get(mSelectedStepIndex)));
        bundle.putLong(Constants.VIDEO_POSITION, mVideoPosition);

        if (null != savedInstanceState) {
            Log.d(TAG, "loadPlayerFragment: called: savedInstanceState is not null");
            mPlayerFragment = getSupportFragmentManager().getFragment(savedInstanceState, Constants.FRAGMENT_STEPS_VIDEO);

            mPlayerFragment.setArguments(bundle);

        } else {
            Log.d(TAG, "loadPlayerFragment: called: savedInstanceState is null");
            mPlayerFragment = new RecipeStepVideoFragment();
            mPlayerFragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.expo_player_container, mPlayerFragment, Constants.FRAGMENT_STEPS_VIDEO).commit();
        }

        idlingResource.decrement();
        IdlingRegistry.getInstance().unregister(idlingResource);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(Constants.SELECTED_RECIPE_KEY, Parcels.wrap(mSelectedRecipe));
        outState.putInt(Constants.SELECTED_RECIPE_INDEX_KEY, mRecipeIndex);
        outState.putString(SELECTED_RECIPE_NAME_KEY, mSelectedRecipe.getName());
        outState.putLong(Constants.VIDEO_POSITION, mVideoPosition);
        Log.d(TAG, "onSaveInstanceState: called");
        try {
            getSupportFragmentManager().putFragment(outState, Constants.PLAYER_FRAGMENT_KEY, mPlayerFragment);
        } catch (NullPointerException e) {
            Log.d(TAG, "onSaveInstanceState: " + e.getMessage());
            mPlayerFragment = null;
        }

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(TAG, "onRestoreInstanceState: called");
        mVideoPosition = savedInstanceState.getLong(Constants.VIDEO_POSITION);
    }

    // Override the onOptionsItemSelected to intercept
    // up navigation and keep the target activity from
    // being destroyed and recreated.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
