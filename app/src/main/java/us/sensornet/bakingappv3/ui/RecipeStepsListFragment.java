package us.sensornet.bakingappv3.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.List;

import us.sensornet.bakingappv3.R;
import us.sensornet.bakingappv3.adapter.RecipeStepsAdapter;
import us.sensornet.bakingappv3.constants.Constants;
import us.sensornet.bakingappv3.model.Recipe;
import us.sensornet.bakingappv3.model.Step;


public class RecipeStepsListFragment extends Fragment implements RecipeStepsAdapter.OnItemClicked {
    private static final String TAG = "RecipeStepsListFragment";

    CountingIdlingResource idlingResource = new CountingIdlingResource("STEPS_LIST_LOADER");

    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    View rootView;
    List<Step> mSteps;
    Recipe mSelectedRecipe;
    Integer mSelectedStepIndex;

    Boolean mIsLargeScreen;
    Boolean mIsLandscape;

    public RecipeStepsListFragment() {
        // Required empty public constructor
    }


    public static RecipeStepsListFragment newInstance() {
        RecipeStepsListFragment fragment = new RecipeStepsListFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        idlingResource.increment();
        IdlingRegistry.getInstance().register(idlingResource);

        // Get Screen orientation and size
        isLandscape();
        isLargeScreen();

        // TODO: Clean up code
        // Get data
        mSelectedRecipe = Parcels.unwrap(getArguments().getParcelable(Constants.SELECTED_RECIPE_KEY));
        mSelectedStepIndex = getArguments().getInt(Constants.SELECTED_STEP_INDEX_KEY);

        if (null == mSelectedRecipe) {
            Log.d(TAG, "onCreate: mSelectedRecipe is null");
        } else {
            Log.d(TAG, "onCreate: mSelectedRecipe is not null");
            mSteps = mSelectedRecipe.getSteps();
            Log.d(TAG, "onCreate: mSelected Step: " + mSelectedRecipe.getSteps().get(mSelectedStepIndex));
        }

        idlingResource.decrement();
        IdlingRegistry.getInstance().unregister(idlingResource);

    }


    private boolean isLargeScreen() {
        mIsLargeScreen = getResources().getConfiguration().isLayoutSizeAtLeast(600);

        return mIsLargeScreen;
    }

    private boolean isLandscape() {
        mIsLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;

        return mIsLandscape;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_recipe_steps_list, container, false);

        idlingResource.increment();
        IdlingRegistry.getInstance().register(idlingResource);

        // Setup RecyclerView
        mRecyclerView = rootView.findViewById(R.id.rv_recipe_steps_list_recycler_view);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new RecipeStepsAdapter(this, mSteps);

        mRecyclerView.setAdapter(mAdapter);

        idlingResource.decrement();
        IdlingRegistry.getInstance().unregister(idlingResource);
        // Inflate the layout for this fragment
        return rootView;
    }


    @Override
    public void onItemClick(int position) {
        Log.d(TAG, "onItemClick: at position: " + String.valueOf(position));

        mSelectedStepIndex = position;

        // Pass the selected recipe to the RecipeStepsDetailActivity
        Bundle bundle = new Bundle();
        bundle.putString(Constants.SELECTED_RECIPE_NAME_KEY, mSelectedRecipe.getName());
        bundle.putParcelable(Constants.SELECTED_RECIPE_KEY, Parcels.wrap(mSelectedRecipe));
        bundle.putInt(Constants.SELECTED_STEP_INDEX_KEY, position);

        // Are we in tablet / Large Screen mode?
        if (getResources().getConfiguration().isLayoutSizeAtLeast(Configuration.SCREENLAYOUT_SIZE_LARGE)) {
            // Start in fragment
            bundle.putString(Constants.VIDEO_URL_KEY, mSteps.get(mSelectedStepIndex).getVideoURL());
            bundle.putString(Constants.THUMBNAIL_URL_KEY, mSteps.get(mSelectedStepIndex).getThumbnailURL());
            bundle.putParcelable(Constants.STEP_KEY, Parcels.wrap(mSteps.get(mSelectedStepIndex)));

            if (mIsLandscape) {
                if (null != bundle) {
                    Fragment stepDetailFragment = new RecipeStepVideoFragment();
                    stepDetailFragment.setArguments(bundle);

                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().add(R.id.expo_player_container, stepDetailFragment, Constants.FRAGMENT_STEPS_VIDEO).commit();
                }
            } else {
                // Start Activity
                Intent stepDetailIntent = new Intent(getActivity(), RecipeStepDetailActivity.class);
                stepDetailIntent.putExtras(bundle);
                startActivity(stepDetailIntent);
            }


        } else {
            // Start Activity
            Intent stepDetailIntent = new Intent(getActivity(), RecipeStepDetailActivity.class);
            stepDetailIntent.putExtras(bundle);
            startActivity(stepDetailIntent);
        }

    }

}
