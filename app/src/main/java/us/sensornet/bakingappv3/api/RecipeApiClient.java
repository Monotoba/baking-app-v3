package us.sensornet.bakingappv3.api;

import android.content.Context;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import us.sensornet.bakingappv3.BuildConfig;
import us.sensornet.bakingappv3.IdlingResources;
import us.sensornet.bakingappv3.model.Recipe;
import us.sensornet.bakingappv3.model.Recipes;

public class RecipeApiClient {
    private static final String TAG = "RecipeApiClient";

    CountingIdlingResource idlingResource = new CountingIdlingResource("API_DATA_LOADER");

    // Url for api
    private final String BASE_URL = "https://d17h27t6h515a5.cloudfront.net/";
    private final List<ServiceInterface> mListeners;
    private RecipeService mService;
    private Retrofit mRetrofit;
    private List<Recipe> mRecipes;

    private Context mContext;

    private int cacheSizeInMB = 10;
    private int cacheSizeInBytes;
    private Cache mCache;

    public RecipeApiClient(Context context, ServiceInterface listener) {
        mContext = context;
        mListeners = new ArrayList<>();
        mListeners.add(listener);

        IdlingRegistry.getInstance().register(idlingResource);
        idlingResource.increment();
        // Build with cache
        OkHttpClient okHttpClient = new OkHttpClient.Builder().cache(mCache).build();

        if (BuildConfig.DEBUG) {
            IdlingResources.registerOkHttp(okHttpClient);
        }

        Retrofit mRetrofit = new Retrofit.Builder().baseUrl(BASE_URL).client(okHttpClient).addConverterFactory(GsonConverterFactory.create()).build();

        RecipeService mService = mRetrofit.create(RecipeService.class);
        idlingResource.decrement();
        IdlingRegistry.getInstance().register(idlingResource);
    }

    public void setCacheSize(int sizeInMB) {
        cacheSizeInMB = sizeInMB;
        cacheSizeInBytes = cacheSizeInMB * (1024 * 1024);
        mCache = new Cache(mContext.getCacheDir(), cacheSizeInBytes);
    }

    public void addListener(ServiceInterface listener) {
        mListeners.add(listener);
    }

    public Recipes requestRecipes() {
        idlingResource.increment();
        Retrofit mRetrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();

        RecipeService mService = mRetrofit.create(RecipeService.class);

        // Fetch a list of recipes
        Call<List<Recipe>> call = mService.getRecipes();

        // Execute the call asynchronously

        call.enqueue(new Callback<List<Recipe>>() {
            @Override
            public void onResponse(Call<List<Recipe>> call, Response<List<Recipe>> response) {
                // The network call was a success and we got a response
                List<Recipe> recipes = response.body();
                mRecipes = recipes;
                doSuccessCallBacks();
            }

            @Override
            public void onFailure(Call<List<Recipe>> call, Throwable t) {
                // The network request was a failure
                Log.d(TAG, "Request Failed: " + t.getMessage());
                doOnFailureCallbacks(t);
            }
        });

        idlingResource.decrement();
        return null;
    }


    public List<Recipe> getData() {
        return mRecipes;
    }

    private void doSuccessCallBacks() {

        for (ServiceInterface listener : mListeners) {
            listener.OnRecipesReady(mRecipes);
        }
    }

    private void doOnFailureCallbacks(Throwable t) {
        for (ServiceInterface listener : mListeners) {
            Log.d(TAG, "doOnFailureCallbacks: called");
            listener.OnRecipeRequestFailure(t);
        }
    }

}
