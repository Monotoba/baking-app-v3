package us.sensornet.bakingappv3.api;

import java.util.List;

import us.sensornet.bakingappv3.model.Recipe;

public interface ServiceInterface {
    void OnRecipesReady(List<Recipe> recipeList);

    void OnRecipeRequestFailure(Throwable t);
}
