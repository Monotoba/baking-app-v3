package us.sensornet.bakingappv3.model;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class Recipes {
    public List<Recipe> mData;

    public Recipes() {
    }

    public Recipes(List<Recipe> data) {
        this.mData = data;
    }

    public List<Recipe> getData() {
        return mData;
    }

    public void setRecipes(List<Recipe> mRecipes) {
        this.mData = mRecipes;
    }
}
