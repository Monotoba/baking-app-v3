package us.sensornet.bakingappv3.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import us.sensornet.bakingappv3.R;
import us.sensornet.bakingappv3.model.Recipe;
import us.sensornet.bakingappv3.repository.RepositoryInterface;

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.ViewHolder> implements RepositoryInterface {
    private static final String TAG = "RecipeListAdapter";

    private CountingIdlingResource idlingResource = new CountingIdlingResource("ADAPTER_DATA_LOADER");

    private List<Recipe> mRecipes;
    private Context mContext;
    private LayoutInflater mInflater;

    private OnItemClicked onClick;

    public RecipeListAdapter(Context mContext) {
        this.mContext = mContext;
        mRecipes = new ArrayList<>();
    }

    public RecipeListAdapter(Context context, List<Recipe> recipeData) {
        mRecipes = recipeData;
        mContext = context;
    }

    public void setOnClick(OnItemClicked onClick) {
        this.onClick = onClick;
    }

    public List<Recipe> getRecipeListData() {
        return mRecipes;
    }

    public void setRecipeListData(List<Recipe> recipes) {
        mRecipes = recipes;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();

        if (null == mInflater) {
            mInflater = LayoutInflater.from(context);
        }

        View view = mInflater.inflate(R.layout.recipe_list_card, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvTitle.setText(mRecipes.get(position).getName());
        holder.tvServings.setText("Servings " + String.valueOf(mRecipes.get(position).getServings()));
        String imageUrl = mRecipes.get(position).getImage();

        holder.cvRecipeCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.onItemClick(position);
            }
        });


        if (!Objects.equals(imageUrl, "")) {
            Uri builtUri = Uri.parse(imageUrl).buildUpon().build();

            IdlingRegistry.getInstance().register(idlingResource);
            idlingResource.increment();
            Picasso.get().load(builtUri).placeholder(android.R.drawable.ic_menu_report_image).into(holder.imgRecipe);
            idlingResource.decrement();
            IdlingRegistry.getInstance().unregister(idlingResource);

        } else {
            holder.imgRecipe.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        int item_count = (mRecipes != null) ? mRecipes.size() : 0;
        return item_count;
    }

    // repository Interface methods
    @Override
    public void OnRepositoryDataReady(List<Recipe> data) {
        mRecipes.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public void OnRepositoryDataRequestFailure(Throwable t) {
        Log.d(TAG, "Error: " + t.getMessage());
    }


    // Click Listener Interface
    public interface OnItemClicked {
        void onItemClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ViewHolder";
        TextView tvTitle;
        TextView tvServings;
        ImageView imgRecipe;
        CardView cvRecipeCard;


        public ViewHolder(View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tv_recipe_title);
            imgRecipe = itemView.findViewById(R.id.img_recipe_image);
            tvServings = itemView.findViewById(R.id.tv_recipe_servings);
            cvRecipeCard = itemView.findViewById(R.id.cv_recipe_card);
        }


    }

}
