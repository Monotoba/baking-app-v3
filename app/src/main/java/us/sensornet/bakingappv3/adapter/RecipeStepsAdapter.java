package us.sensornet.bakingappv3.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import us.sensornet.bakingappv3.R;
import us.sensornet.bakingappv3.model.Step;

public class RecipeStepsAdapter extends RecyclerView.Adapter<RecipeStepsAdapter.ViewHolder> {
    private static final String TAG = "RecipeStepsAdapter";

    CountingIdlingResource idlingResource = new CountingIdlingResource("STEPS_DATA_LOADER");

    private List<Step> mSteps;
    private LayoutInflater mInflater;
    private OnItemClicked onClick;

    public RecipeStepsAdapter(OnItemClicked onclick, List<Step> mSteps) {

        this.mSteps = mSteps;
        this.onClick = onclick;
    }

    // Set the callback object
    public void setOnClick(OnItemClicked onClick) {
        this.onClick = onClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();

        if (null == mInflater) {
            mInflater = LayoutInflater.from(context);
        }

        View view = mInflater.inflate(R.layout.steps_list_card, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        IdlingRegistry.getInstance().register(idlingResource);
        idlingResource.increment();

        Step curStep = mSteps.get(position);
        String stepHeader = "Step " + String.valueOf(curStep.getId()) + " of " + String.valueOf(mSteps.size() - 1) + ": " + curStep.shortDescription;

        holder.tvShortDesc.setText(stepHeader);
        holder.tvStepDesc.setText(curStep.description);


        if (!curStep.thumbnailURL.isEmpty() && curStep.thumbnailURL != "" && null != curStep.thumbnailURL) {
            Picasso.get().load(curStep.getThumbnailURL().toString()).placeholder(android.R.drawable.ic_menu_report_image).into(holder.imgStep);
        } else {
            holder.imgStep.setVisibility(View.GONE);
        }

        // Pack urls into holder
        if (!curStep.getVideoURL().isEmpty() && null != curStep.getVideoURL()) {
            holder.cvStepCard.setTag(R.id.step_video_url_id, curStep.getVideoURL());
            holder.cvStepCard.setTag(R.id.step_thumbnail_id, curStep.getThumbnailURL());
        }

        // Setup click handler
        holder.cvStepCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.onItemClick(position);
            }
        });

        idlingResource.decrement();
        IdlingRegistry.getInstance().unregister(idlingResource);

    }

    @Override
    public int getItemCount() {
        return (null == mSteps) ? 0 : mSteps.size();
    }


    // Create the interface for click handling
    public interface OnItemClicked {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "Steps-ViewHolder";

        private TextView tvShortDesc;
        private TextView tvStepDesc;
        private ImageView imgStep;
        private ConstraintLayout cvStepCard;

        public ViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "Constructor called");

            tvShortDesc = itemView.findViewById(R.id.tv_short_desc);
            tvStepDesc = itemView.findViewById(R.id.tv_step_desc);
            imgStep = itemView.findViewById(R.id.img_step_image);
            cvStepCard = itemView.findViewById(R.id.step_card_container);
            if (null == cvStepCard) {
                Log.d(TAG, "ViewHolder: cvStepCard is null");
            } else {
                Log.d(TAG, "ViewHolder: cvStepCard is not null");
            }
        }
    }
}
