package us.sensornet.bakingappv3.repository;

import java.util.List;

import us.sensornet.bakingappv3.model.Recipe;

public interface RepositoryInterface {
    void OnRepositoryDataReady(List<Recipe> data);

    void OnRepositoryDataRequestFailure(Throwable t);
}

