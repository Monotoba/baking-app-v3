package us.sensornet.bakingappv3.repository;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import us.sensornet.bakingappv3.api.RecipeApiClient;
import us.sensornet.bakingappv3.api.ServiceInterface;
import us.sensornet.bakingappv3.model.Recipe;

public class RecipeRepository implements ServiceInterface {
    private static final String TAG = "RecipeRepository";
    private static List<Recipe> mRecipes;
    private final List<RepositoryInterface> mListeners;
    private RecipeApiClient mClient;

    private Context mContext;

    public RecipeRepository(Context context) {

        // Initialize listeners
        mListeners = new ArrayList<>();
        // Make Api request to get data
        mClient = new RecipeApiClient(mContext, this);
        mClient.requestRecipes();

        // Create an empty recipe array so
        // we don't have null pointer issues
        mRecipes = new ArrayList<Recipe>();

        // Make API Call to retrieve data from network
        getApiData();
    }

    public RecipeRepository(RepositoryInterface listener) {
        Log.d(TAG, "RecipeRepository Constructor called with listener");
        // Add listener
        mListeners = new ArrayList<>();
        this.mListeners.add(listener);

        // Make Api request to get data
        mClient = new RecipeApiClient(mContext, this);
        mClient.requestRecipes();

        // Create an empty recipe array so
        // we don't have null pointer issues
        mRecipes = new ArrayList<Recipe>();
    }

    // Sets the data
    public static List<Recipe> getData() {
        return mRecipes;
    }

    private void getApiData() {
        // Make Api request
        mClient = new RecipeApiClient(mContext, this);
        mClient.requestRecipes();
    }

    @Override
    public void OnRecipesReady(List<Recipe> recipeList) {
        mRecipes = recipeList;
        notifyDataReady();
    }

    @Override
    public void OnRecipeRequestFailure(Throwable t) {
        Log.d(TAG, "Error in OnRecipeRequestFailure: " + t.getMessage());
        notifyDataRequestFailed(t);
    }

    private void notifyDataReady() {
        for (RepositoryInterface listener : mListeners) {
            listener.OnRepositoryDataReady(mRecipes);
        }
    }

    private void notifyDataRequestFailed(Throwable error) {
        Log.d(TAG, "notifyDataRequestFailed and calling Listeners");
        for (RepositoryInterface listener : mListeners) {
            listener.OnRepositoryDataRequestFailure(error);
        }

    }
}
