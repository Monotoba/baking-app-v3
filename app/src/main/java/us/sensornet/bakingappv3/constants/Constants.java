package us.sensornet.bakingappv3.constants;

public class Constants {
    public static final String RECIPES_KEY = "ALL_RECIPES";
    public static final String SELECTED_RECIPE_KEY = "SELECTED_RECIPE";
    public static final String SELECTED_RECIPE_NAME_KEY = "SELECTED_RECIPE_NAME";
    public static final String SELECTED_RECIPE_INDEX_KEY = "SELECTED_RECIPE_INDEX";
    public static final String SELECTED_STEP_INDEX_KEY = "SELECTED_STEP_INDEX";

    public static final String INGREDIENTS_KEY = "INGREDIENTS";

    public static final String STEPS_KEY = "STEPS";
    public static final String STEP_KEY = "STEP";

    public static final String THUMBNAIL_URL_KEY = "THUMBNAIL_URL";
    public static final String VIDEO_URL_KEY = "VIDEO_URL";

    public static final String BACKSTACK_INGREDIENTS_KEY = "BACKSTACK_INGREDIENTS_KEY";
    public static final String BACKSTACK_STEPS_KEY = "BACKSTACK_STEPS_KEY";

    // Fragment TAGS
    public static final String FRAGMENT_RECIPE_LIST = "RECIPE_LIST_FRAGMENT";
    public static final String FRAGMENT_INGREDIENTS_LIST = "RECIPE_INGREDIENTS_LIST";
    public static final String FRAGMENT_STEPS_LIST = "RECIPE_STEPS_LIST";
    public static final String FRAGMENT_STEPS_VIDEO = "STEPS_VIDEO";

    public static final String SHARED_PREFERENCE_FILE = "us.sensornet.bakingappv3.BAKING_APP_V3";

    // For Use With ExoPlayer
    public static final String VIDEO_POSITION = "VIDEO_POISTION";

    public static final String PLAYER_FRAGMENT_KEY = "PLAYER_FRAGMENT";
    public static final String PLAYER_STATE_KEY = "PLAYER_STATE";
    public static final String PLAYER_WINDOW_KEY = "PLAYER_WINDOW_KEY";

}
